//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <string_view>
#include <variant>

#include <container/span.h>
#include <fs/fs.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        struct memory_resource
        {
            std::vector<std::byte> data;
        };

        struct memory_view_resource
        {
            span<const std::byte> data;
        };

        struct path_resource
        {
            fs::path path;
        };

        using resource = std::variant<
            memory_resource,
            memory_view_resource,
            path_resource
        >;

        template <class MemoryVisitor, class PathVisitor>
        auto visit_resource(const gfx::resource & resource, MemoryVisitor && m, PathVisitor && p) {
            return std::visit(overloaded{
                [&](const memory_resource & r) {
                    m(r.data);
                },
                [&](const memory_view_resource & r) {
                    m(r.data);
                },
                [&](const path_resource & r) {
                    p(r.path);
                }
            }, resource);
        }

        template <class MemoryVisitor>
        auto evaluate_resource(const gfx::resource & resource, MemoryVisitor && m) {
            return std::visit(overloaded{
                [&](const memory_resource & r) {
                    m(r.data);
                },
                [&](const memory_view_resource & r) {
                    m(r.data);
                },
                [&](const path_resource & r) {
                    m(fs::read_bytes(r.path));
                }
            }, resource);
        }
    }
}
