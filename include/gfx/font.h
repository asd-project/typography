//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <compare>
#include <boost/container_hash/hash.hpp>

#include <container/map.h>
#include <container/span.h>
#include <container/enum_index_array.h>

#include <gfx/gfx.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        using font_id = gfx::object_id<class font_tag>;

        struct font
        {
            font_id id;
            u32 size = 0;

            friend std::strong_ordering operator <=> (const font &, const font &) = default;
        };

        enum class font_style
        {
            regular,
            italic
        };

        struct font_weight
        {
            constexpr font_weight(u16 value) noexcept : value(value) {}

            constexpr operator u16 () const noexcept {
                return value;
            }

            friend constexpr std::strong_ordering operator <=> (font_weight, font_weight) = default;

            u16 value = font_weight::regular;

            enum : u16
            {
                thin = 100,
                extra_light = 200, ultra_light = 200,
                light = 300,
                regular = 400, normal = 400,
                medium = 500,
                semi_bold = 600,
                bold = 700,
                extra_bold = 800, ultra_bold = 800,
                black = 900,
                extra_black = 950, ultra_black = 950,
            };
        };

        struct font_variant
        {
            font_style style = font_style::regular;
            font_weight weight = font_weight::regular;

            friend constexpr std::strong_ordering operator <=> (const font_variant &, const font_variant &) noexcept = default;
        };

        struct font_family
        {
            font_family() noexcept {}
            font_family(font_family &&) noexcept = default;

            font_family(ordered_map<font_variant, font_id> && fonts) noexcept :
                fonts(std::move(fonts)) {}

            font_family & operator = (font_family &&) noexcept = default;

            ordered_map<font_variant, font_id> fonts;
        };

        struct font_table
        {
            font_table() noexcept {}
            font_table(font_table &&) noexcept = default;

            font_table(std::vector<font_family> && families) noexcept :
                families(std::move(families)) {}

            font_table & operator = (font_table &&) noexcept = default;

            void add_family(font_family family) {
                families.emplace_back(std::move(family));
            }

            void query_fonts(std::vector<font_id> & output, font_variant variant) {
                for (auto & family : families) {
                    auto it = family.fonts.lower_bound(variant);

                    if (it != family.fonts.end() && it->first.style == variant.style) {
                        output.push_back(it->second);
                    }
                }
            }

            std::vector<font_family> families;
        };
    }
}

namespace std
{
    template <>
    struct hash<::asd::gfx::font>
    {
        constexpr size_t operator()(const ::asd::gfx::font & font) const {
            return ::asd::hash_combine(std::pair{font.id, font.size});
        }
    };
}
