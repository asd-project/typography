//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/font.h>
#include <gfx/resource.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class Context>
        class font_source
        {
        public:
            gfx::font_id load(const gfx::resource & resource) = delete;
        };
    }
}
