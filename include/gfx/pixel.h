//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/common.h>
#include <math/math.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        using px = f32;

        namespace literals
        {
            constexpr gfx::px operator ""_px(unsigned long long p) noexcept {
                return static_cast<f32>(p);
            }

            constexpr gfx::px operator ""_px(long double p) noexcept {
                return static_cast<f32>(p);
            }
        }

        using namespace ::asd::gfx::literals;
    }
}
