//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <string_view>
#include <string>

#include <gfx/font.h>

#if defined(TYPOGRAPHY_USE_BOOST_TEXT)
#include <boost/text/text.hpp>
#include <boost/text/grapheme.hpp>
#elif defined(TYPOGRAPHY_USE_UTFCPP)
#include <utf8cpp/utf8.h>
#else
#pragma message("Can not use utf8 transcoding, fallback to ASCII typography")
#endif

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
#if defined(TYPOGRAPHY_USE_BOOST_TEXT)
        using text = boost::text::text;
        using text_view = boost::text::text_view;

        inline auto text_begin(const gfx::text_view & text) {
            return text.begin();
        }

        inline auto text_end(const gfx::text_view & text) {
            return text.end();
        }

        inline u32 text_code_point(auto iterator) {
            return *iterator.base();
        }

        inline gfx::text_view make_text_view(auto begin, auto end) {
            return {begin, end};
        }

        inline i32 text_length(const gfx::text_view & text) {
            return static_cast<i32>(text.storage_code_units());
        }

        inline const char * text_data(const gfx::text_view & text) {
            return text.begin().base().base();
        }

#elif defined(TYPOGRAPHY_USE_UTFCPP)

        using text = std::string;
        using text_view = std::string_view;

        inline auto text_begin(const gfx::text_view & text) {
            return utf8::iterator(text.begin(), text.begin(), text.end());
        }

        inline auto text_end(const gfx::text_view & text) {
            return utf8::iterator(text.end(), text.begin(), text.end());
        }

        inline u32 text_code_point(auto iterator) {
            return *iterator;
        }

        inline gfx::text_view make_text_view(auto begin, auto end) {
            return {begin.base(), end.base()};
        }

        inline i32 text_length(const gfx::text_view & text) {
            return static_cast<i32>(text.size());
        }

        inline const char * text_data(const gfx::text_view & text) {
            return reinterpret_cast<const char *>(text.data());
        }
#else
        using text = std::string;
        using text_view = std::string_view;

        inline auto text_begin(const gfx::text_view & text) {
            return text.begin();
        }

        inline auto text_end(const gfx::text_view & text) {
            return text.end();
        }

        inline u32 text_code_point(auto iterator) {
            return *iterator;
        }

        inline gfx::text_view make_text_view(auto begin, auto end) {
            return {begin, end};
        }

        inline i32 text_length(const gfx::text_view & text) {
            return static_cast<i32>(text.size());
        }

        inline const char * text_data(const gfx::text_view & text) {
            return reinterpret_cast<const char *>(text.data());
        }
#endif

        struct text_fragment
        {
            gfx::text_view text;
            gfx::font_id font_id;
        };
    }
}
