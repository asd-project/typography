//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <vector>

#include <gfx/text.h>
#include <gfx/glyph.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class Context>
        class text_shaper
        {
        public:
            gfx::pixel_size shape(std::vector<glyph_slot> & glyph_slots, const gfx::text_fragment & fragment, u32 size, gfx::pixel_point offset = {0, 0}) = delete;
        };
    }
}
