//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/rect.h>

#include <gfx/gfx.h>
#include <gfx/texture.h>

#include <gfx/unit.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        using glyph_id = gfx::object_id<class glyph_tag>;

        using pixel_point = math::point<gfx::px>;
        using pixel_size = math::size<gfx::px>;
        using pixel_area = math::rect<gfx::px>;

        struct glyph
        {
            math::int_rect local_area;
            math::uint_rect atlas_area;
        };

        struct glyph_slot
        {
            glyph_id id;
            gfx::pixel_area area;
            gfx::glyph * glyph = nullptr;
        };

        using glyph_range = span<glyph_slot>;
    }
}
