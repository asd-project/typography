//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <vector>

#include <container/span.h>
#include <math/rect.h>

#include <gfx/gfx.h>
#include <gfx/texture.h>

#include <gfx/glyph.h>
#include <gfx/font.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class Context, class Gfx>
        struct font_atlas {};

        template <class Context, class Gfx>
        struct font_atlas_storage {};

        enum class raster_font_format
        {
            grayscale,
            colored
        };

        struct raster_font_data
        {
            raster_font_data(gfx::font font, raster_font_format preferred_format = raster_font_format::grayscale) :
                font(font), preferred_format(preferred_format) {}

            raster_font_data(const raster_font_data &) = default;

            gfx::font font;
            raster_font_format preferred_format;
        };

        template <class Context, class Gfx>
        class font_rasterizer
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            // return atlas texture
            gfx::texture<Gfx> & rasterize(const glyph_range & glyphs, const raster_font_data & font_data) = delete;
        };
    }
}
