import os

from conans import ConanFile, CMake

project_name = "typography"


class TypographyConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Abstract components for font rendering and text shaping"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.gfx/0.0.1@asd/testing",
        "asd.fs/0.0.1@asd/testing",
    )
    options = {
        "use_boost_text": [True, False],
    }
    default_options = {
        "use_boost_text": False
    }

    def requirements(self):
        if self.options.use_boost_text:
            self.requires("boost_text/1.0.0@asd/testing")
        else:
            self.requires("utfcpp/3.2.1")

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy("*.h", dst="include", src="include")

    def package_info(self):
        if self.options.use_boost_text:
            self.cpp_info.defines = ["TYPOGRAPHY_USE_BOOST_TEXT"]
        else:
            self.cpp_info.defines = ["TYPOGRAPHY_USE_UTFCPP"]
